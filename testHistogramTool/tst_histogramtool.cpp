#include <QString>
#include <QtTest>
#include <QImage>
#include <QRandomGenerator>
#include "../histogramTool/Lib/colorhist.h"
class TestHistogramToolTest : public QObject
{
    Q_OBJECT

public:
    TestHistogramToolTest();

private Q_SLOTS:
    void testCase1();
};

TestHistogramToolTest::TestHistogramToolTest()
{
    QSize tst_img_size = QSize(3,4);
    QImage tst_img = QImage(tst_img_size, QImage::Format_RGB32);
    tst_img.fill(QColor(100, 0, 0));
    ColorHist tst_col_hist(tst_img);
    tst_col_hist.calc_color_hist_multithread();
    RgbHist result = tst_col_hist.get_rgb_hist();
    QVERIFY(result.data[0][99] == 12);
    QRandomGenerator rndgn;
    int bin = rndgn.bounded(256);
    int ch = rndgn.bounded(3);
    if (bin != 99 && ch != 0)
        QVERIFY(result.data[ch][bin] == 0);
}

void TestHistogramToolTest::testCase1()
{
    QVERIFY2(true, "Failure");
}

QTEST_APPLESS_MAIN(TestHistogramToolTest)

#include "tst_histogramtool.moc"
