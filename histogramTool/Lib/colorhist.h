#ifndef COLORHIST_H
#define COLORHIST_H

#include <QVector>
#include <QString>
#include <QImage>

const int NUM_BINS = 256;
const char NUM_CH = 3;

class RgbHist
{
public:
    QVector<QVector<int>> data;
    RgbHist();
    RgbHist operator+ (const RgbHist&) const;
    RgbHist & operator=(const RgbHist& rhs);

};

struct ImageChunk
{
  QRgb *data;
  int width;
};

class ColorHist
{
public:
    ColorHist(const QString&);
    ColorHist(const QImage&);
    int calc_color_hist_singlethread();
    int write_hist_to_csv(const QString&);
    int calc_color_hist_multithread();
    RgbHist get_rgb_hist();
    int get_pixel_count();
private:
    QImage img;
    QRgb *st;
    RgbHist rgbHist;
    quint64 pixelCount;
    void increment_hist(QRgb&);

};



#endif // COLORHIST_H
