#include "colorhist.h"
#include <QFile>
#include <QTextStream>
#include <QtConcurrent>
#include <iostream>

RgbHist::RgbHist()
{
    data.resize(NUM_CH);
    for(int c=0; c<NUM_CH; ++c)
        data[c].resize(NUM_BINS);
}

RgbHist RgbHist::operator+ (const RgbHist& h) const
{
      RgbHist result;
      for(int c=0; c<NUM_CH; ++c)
      {
          for(int p=0; p<NUM_BINS; ++p)
              result.data[c][p] = this->data[c][p] + h.data[c][p];
      }
      return result;
}

RgbHist & RgbHist::operator=(const RgbHist & rhs)
{
    if(this == &rhs)
       return *this;
    for(int c=0; c<NUM_CH; ++c)
        this->data[c] = rhs.data[c];
    return *this;
}

ColorHist::ColorHist(const QString& filename)
{

    img.load(filename);
    st = (QRgb*) img.bits();
    pixelCount = img.width() * img.height();
}

ColorHist::ColorHist(const QImage& _img )
{
    img = _img;
    st = (QRgb*) img.bits();
    pixelCount = img.width() * img.height();
}

int ColorHist::calc_color_hist_singlethread()
{
    for (quint64 p = 0; p < pixelCount; ++p)
        increment_hist(*(st+p));
    return 0;
}

void ColorHist::increment_hist(QRgb& color)
{
    ++rgbHist.data[0][qRed(color)];
    ++rgbHist.data[1][qGreen(color)];
    ++rgbHist.data[2][qBlue(color)];
    return;
}

RgbHist calc_row_hist(const ImageChunk &row)
{
  RgbHist result;
  for(int i=0;i<row.width;++i)
  {
    ++result.data[0][qRed(row.data[i])];
    ++result.data[1][qGreen(row.data[i])];
    ++result.data[2][qBlue(row.data[i])];
  }
  return result;
}

void populate_row_hists(RgbHist& result, const RgbHist &row_result)
{
    result = result + row_result;
}

int ColorHist::calc_color_hist_multithread()
{
    QList<ImageChunk> rows;
    for(int i=0;i<img.height();++i){
        ImageChunk row;
        row.data = (QRgb*)img.scanLine(i);
        row.width = img.width();
        rows << row;
    }
    RgbHist total_result = QtConcurrent::blockingMappedReduced(rows, calc_row_hist, populate_row_hists);
    rgbHist = total_result;

    return 0;
}
int ColorHist::write_hist_to_csv(const QString& filename)
{
    QFile file(filename);
    if (file.open(QIODevice::ReadWrite))
    {
        QTextStream stream(&file);
        for (int c = 0; c < NUM_CH; ++c)
        {
            for (auto i = rgbHist.data[c].begin(); i != rgbHist.data[c].end(); ++i)
                stream << *i << ',';
            stream <<'\n';
        }

    }
    else
        return -1;

    return 0;
}

RgbHist ColorHist::get_rgb_hist()
{
    return rgbHist;
}

int ColorHist::get_pixel_count()
{
    return pixelCount;
}
