#include <QCoreApplication>
#include <QStringList>
#include <QCommandLineParser>
#include "Lib/colorhist.h"
#include <QTime>
#include <iostream>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QCommandLineParser parser;

    parser.addPositionalArgument("source", QCoreApplication::translate("main", "Input image file to read."));
    parser.addPositionalArgument("destination", QCoreApplication::translate("main", "Ouput csv file path to write to."));
    parser.process(a);

    // get the commandline arguments
    const QStringList args = parser.positionalArguments();
    if (args.length() <2)
    {
        std::cerr << "Error: insufficient number of arguments"<<std::endl;
        return -1;
    }

    ColorHist col_hist(args.at(0));
    if(col_hist.get_pixel_count() == 0)
    {
        std::cerr << "Error loading image." <<std::endl;
        return -1;
    }

    QTime t;
    std::cout<<"calculating color histogram with a single thread ..."<<std::endl;
    t.start();
    col_hist.calc_color_hist_singlethread();
    qDebug("Time elapsed with single thread: %d ms", t.elapsed());

    t.start();
    std::cout<<"calculating color histogram with multiple threads ..."<<std::endl;
    col_hist.calc_color_hist_multithread();
    qDebug("Time elapsed with multiple threads: %d ms", t.elapsed());

    std::cout<<"writing to csv ..."<<std::endl;
    int write_status = col_hist.write_hist_to_csv(args.at(1));
    if (write_status<0)
        std::cerr << "unable to write to: " << args.at(1).toStdString() <<std::endl;
    else
        std::cout<<"writing to csv completed"<<std::endl;

    return 0;
}
